package com.thinkcoer.controller;

import com.thinkcoer.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
@Controller
public class LoginController {

    @ResponseBody
    @PostMapping("/login")
    public String login(@RequestBody User user, HttpServletRequest request){
        HttpSession session = request.getSession();

        if(!user.getName().equals("root")&&!user.getPwd().equals("root")){
            return "用户名或者密码错误！";
        }
        session.setAttribute("user",user);
        return "登录成功";
    }

    @ResponseBody
    @GetMapping("/test")
    public void loginTest(){
        log.info("handler方法执行");
    }


    @GetMapping("/loginview")
    public ModelAndView getView(){
        return new ModelAndView("login");
    }
}

package com.thinkcoer.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;


@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean authFilterRegistation(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        //注册bean
        registrationBean.setFilter(new AuthFilter());
        //设置bean name
        registrationBean.setName("AuthFilter");
        //拦截所有请求
        registrationBean.addUrlPatterns("/*");
        //执行顺序，数字越小优先级越高
        registrationBean.setOrder(1);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean logFilterRegistation(){
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new LogFilter());
        registrationBean.setName("LogFilter");
        registrationBean.addUrlPatterns("/*");
        registrationBean.setOrder(2);
        return registrationBean;
    }
}

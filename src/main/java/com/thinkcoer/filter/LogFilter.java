package com.thinkcoer.filter;


import com.thinkcoer.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.servlet.*;
import java.io.IOException;

@Slf4j
public class LogFilter implements Filter {


    @Override
    public void init(javax.servlet.FilterConfig filterConfig) throws ServletException {
        log.info("日志filter init方法执行");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.info("日志doFilter方法执行");
        log.info("处理业务逻辑,改变请求体对象和回复体对象");
        //调用filter链中的下一个filter
        filterChain.doFilter(servletRequest,servletResponse);
    }


    @Override
    public void destroy() {
        log.info("日志filter destroy方法执行");
    }
}

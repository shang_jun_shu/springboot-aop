package com.thinkcoer.filter;

import lombok.extern.slf4j.Slf4j;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//@Slf4j
//@WebFilter(urlPatterns = "/*",filterName = "LoginFilter",initParams = {
//        @WebInitParam(name="includeUrls",value = "/login")
//})
//public class LoginFilter implements Filter {
//
//    //不需要登录就可以访问的路径(比如:注册登录等)
//    private String includeUrls;
//
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        HttpSession session = request.getSession();
//        String uri = request.getRequestURI();
//
//        System.out.println("filter url:"+uri);
//
//        if (uri.equals(includeUrls)) { //不需要过滤直接传给下一个过滤器
//            filterChain.doFilter(servletRequest, servletResponse);
//        } else {
//            //需要过滤器
//            // session中包含user对象,则是登录状态
//            if(session!=null&&session.getAttribute("user") != null){
//                System.out.println("user:"+session.getAttribute("user"));
//                filterChain.doFilter(request, response);
//            }else{
//                response.setContentType("Application/json;charset=UTF-8");
//                response.getWriter().write("您还未登录");
//                //重定向到登录页(需要在static文件夹下建立此html文件)
//                //response.sendRedirect(request.getContextPath()+"/user/login.html");
//                return;
//            }
//        }
//    }
//
//
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//        //获取初始化filter的参数
//        this.includeUrls=filterConfig.getInitParameter("includeUrls");
//    }
//
//    @Override
//    public void destroy() {
//        log.info("loginfilter销毁方法执行了");
//    }
//}

package com.thinkcoer.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //需要拦截的路径，/**表示拦截所有请求
        String[] addPathPatterns={"/**"};
        //不需要拦截的路径
        String[] excludePathPatterns={"/boot/login","/boot/exit"};

        registry.addInterceptor(new AuthIntercepter())
                .addPathPatterns(addPathPatterns)
                .excludePathPatterns(excludePathPatterns);
        //新注册的过滤器
        registry.addInterceptor(new LogInterceptor())
                .addPathPatterns(addPathPatterns)
                .excludePathPatterns(excludePathPatterns);
    }
}

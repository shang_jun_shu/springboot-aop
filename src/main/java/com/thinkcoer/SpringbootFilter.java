package com.thinkcoer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SpringbootFilter {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootFilter.class);
    }
}
